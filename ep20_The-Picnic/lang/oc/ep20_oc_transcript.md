# Transcript of Pepper&Carrot Episode 20 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 20 : Lo Picnic

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|- FIN -
Crèdits|2|False|12/2016 - www.peppercarrot.com - Dessenh & Scenari : David Revoy
Crèdits|3|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Crèdits|4|False|Licéncia : Creative Commons Attribution 4.0, Logicials : Krita 3.1, Inkscape 0.91 sus Manjaro XFCE

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 825 mecènas :
Crèdits|2|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
