# Transcript of Pepper&Carrot Episode 38 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Episode 38: The Healer

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Finally...
Vinya|2|False|We managed to finish off this one!
Fritz|3|False|Yes, it was difficult. Now we understand why so many adventurers were stuck there.
Fritz|4|False|How is Brasic?
Fritz|5|False|Brasic? Brasic!
Fritz|6|False|Damn! I didn't notice that you were so badly hurt!
Vinya|7|False|Healer!
Vinya|8|False|We need your help, FAST!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Step aside...
Son|2|False|Dzing
Brasic|3|False|My wounds are gone.
Brasic|4|False|Every time it's the same miracle.
Brasic|5|False|Your talent is incredible, Healer!
Brasic|6|False|Let's not delay. The treasure is waiting for us!
Fritz|7|False|Hear hear!
Fritz|8|False|Thanks to her, we're invincible!
Fritz|9|False|Let's go find the next monster!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|Dibs!
Vinya|2|False|No, me! Me!
Pepper|3|False|?
Cayenne|4|False|What do you think you're doing?
Cayenne|5|False|We've been looking for you for months.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Isn't it obvious?
Pepper|2|False|I decided to give it all up and change my career.
Pepper|3|False|From now on, I am the Great Healer Peph'Ra , as you can see.
Cayenne|4|False|...
Pepper|5|False|"But why" , you might ask?!
Pepper|6|False|It's simple!
Pepper|7|False|I HAVE HAD IT!
Pepper|8|False|I'm sick of being hunted, sick of being laughed at, and above all, I'm sick of not being able to find work after studying for so long!
Pepper|9|False|At least here I'm needed and appreciated for my skills!
Brasic|10|False|Healer!
Brasic|11|False|Don't stray too far!
Brasic|12|False|Vinya took a nasty blow.
Son|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|That's it.
Pepper|2|False|Anything else?
Cayenne|3|False|Yes.
Cayenne|4|False|Having your name on a wanted poster is normal for a Chaosah witch.
Cayenne|5|False|It's even the sign of a good reputation.
Cayenne|6|False|Ignore the nay-sayers and what-have-you , and create your own career if nobody gives you one.
Cayenne|7|False|And above all: you are not a healer.
Cayenne|8|False|They'll notice, sooner or later, when you've run out of that incredible amount of Phoenix Tears you're carrying around, pretending you have new powers.
Pepper|9|False|Wha... ?
Pepper|10|False|How did you...
Pepper|11|False|...guess.
Cayenne|12|False|Tsk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr!!!
Pepper|2|False|Well, if I need more I'll go get more!
Pepper|3|False|Because THIS WORKS!
Pepper|4|False|Unlike all of your twisted Chaosah stuff.
Pepper|5|False|G R RR ...!
Brasic|6|False|Watch out!
Brasic|7|False|The monster is shape-shifting!
Monstre|8|False|CRe ee e ee eee
Monstre|9|False|ee ee ee eee e ee e ee

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstre|1|False|CRe eee eee eee eee ee e ee
Monstre|2|False|ee e eee e ee ee e e eeee
Monstre|3|False|ee e eee e eeeeeee e eeeee ee eeee ee eeeee eeee
Son|4|False|CR R R R ! ! !
Son|5|False|CR R R R ! ! !
Son|6|False|CR R R R ! ! !
Son|7|False|Crack ! ! !
Son|8|False|Crack ! ! !
Son|9|False|Crack ! ! !
Son|10|False|Crack ! ! !
Son|11|False|Crack ! ! !
Son|12|False|Crack ! ! !
Monstre|13|False|e ee ee e e e...
Son|14|False|Dze ey o o !!
Vinya|15|False|Oh my, what a ruckus!
Brasic|16|False|Cover me. I'm going to silence it for good!
Brasic|17|False|You hear that, monster?!
Brasic|18|False|Keep an eye on me just in case, Healer...
Brasic|19|False|Healer?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|OOPS !
Pepper|2|False|No, no, no!
Brasic|3|False|Rho ho ho!
Brasic|4|False|Look at that! I think the beast's cry was too much for our healer's constitution.
Brasic|5|False|Bwa ha ha!
Fritz|6|False|HAHA HA HA!
Fritz|7|False|Not only that, but it's all blue!
Vinya|8|False|HAHA HA HA!
Vinya|9|False|And you think it can heal, too?
Fritz|10|False|Ugh, gross!
Fritz|11|False|HAHA HA HA!
Brasic|12|False|BWA HA HA!
Pepper|13|False|...
Fritz|14|False|HEY!
Brasic|15|False|Wait! We were just joking!
Vinya|16|False|Healer!
Cayenne|17|False|Welcome back.
Titre|18|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|April $$, 2023 Art & scenario: David Revoy. Beta readers: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. English translation: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, CitizenK4te, Craig Maloney, Estefania de Vasconcellos Guimaraes, Frederic Mora, Jelfo, Marno van der Maas. Proofreading: H-Games, Menhir. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. License: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Did you know?
Pepper|3|False|Pepper&Carrot is entirely free (libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to %%%% patrons!
Pepper|5|False|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|6|False|We are on Patreon, Tipeee, PayPal, Liberapay ... and more!
Pepper|7|False|Check www.peppercarrot.com for more info!
Pepper|8|False|Thank you!
Notes|9|False|You can also translate this page if you want.
Notes|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Notes|11|False|Beta readers help with the story, proofreaders give feedback about the text.
