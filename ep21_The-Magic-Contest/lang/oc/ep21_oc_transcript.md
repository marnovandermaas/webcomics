# Transcript of Pepper&Carrot Episode 21 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 21 : Lo Concors de Magia

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|1|False|A la demanda generala, la vila de Komona presenta lo Concors de Magia
Écriture|2|False|Grand Prèmi de 50 000Ko
Écriture|3|False|Per la demostracion magica mai impressionanta ! Entrada : 150Ko Arenas novèlas gigantas e securizadas Komona Diazard, 10h del satin
Écriture|4|False|INVITACION ESPECIALA
Pepper|5|False|Soi prèsta ! Anem, Carròt. I anam !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Sètz totjorn seguras de voler pas venir amb nosautres ?
Cayenne|2|False|Solidas. Fasèm de mascariá a Caosah, pas d'espectacle !
Écriture|3|False|Lo KOMONAN
Écriture|4|False|INAUGURACION DE LAS ARENAS NOVÈLAS UÈI
Écriture|5|False|D'ESTATJANTS PREOCUPATS PER LORS IMPÒSTES
Écriture|6|False|VENTA A GUICHET BARRAT : ES UNA POLIDA CAPITADA !
Écriture|7|False|TENSIONS POLITICAS EN TÈRRA DE AH
Écriture|8|False|AH OCUPA ARA 80% DELS TERRITÒRIS
Écriture|9|False|AVÈTZ UNA BONA VISTA !
Pepper|10|True|Mercé de me daissar participar !
Pepper|11|True|Ganharai ! Promés !
Pepper|12|False|A tot ara !
Cayenne|14|False|...
Son|13|False|Flau !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
Public|2|False|Clap
Cònsol de Komona|1|False|Coma cònsol de Komona, declari lo segond Grand Concors de Magia obèrt !
Cònsol de Komona|3|True|Sabi qu'esperatz aqueste eveniment impacientament !
Cònsol de Komona|4|False|Mercé de vos èstre desplaçats dels quatre cantons d'Hereva per descobrir nòstras grandas arenas subresecurizadas !
Cònsol de Komona|5|False|Passam ara a la presentacion de las participantas !
Carròt|6|True|Z
Carròt|7|True|Z|nowhitespace
Carròt|8|False|Z ...|nowhitespace
Pepper|9|True|Carròt ?! Mas qué fas ?!
Pepper|10|True|Desvelha-te ! Aquò comença !
Pepper|11|False|E tot lo monde nos gaita !
Son|12|False|Pòc !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
Public|3|False|Clap
Cònsol de Komona|1|True|Adèpta de la magia mortalament reviscolanta de « Zombiah », avèm l'onor d'aculhir...
Cònsol de Komona|2|False|CORIANDRE !
Cònsol de Komona|4|True|La fauna e la flòra an pas de secret per aquela masca d'« Hippiah » ! Sètz pregats d'aplaudir...
Cònsol de Komona|5|False|CAMOMILHA !
Cònsol de Komona|6|False|E ara, mercés a las proesas tecnicas de Komona, vaquí, en exclusivitat mondiala...
Son|7|False|Flop !
Cònsol de Komona|8|True|...una masca dels fonzes marins que mestreja la magia redobtabla d'« Aquah » !
Cònsol de Komona|9|False|ESPIRULINA !
Pepper|10|False|Aquah ?! Aquí ?! Incredible !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
Public|9|False|Clap
Carròt|1|False|Gro o o o|nowhitespace
Pepper|2|True|Carròt !
Pepper|3|True|Es lèu a nosautres !
Pepper|4|True|Siá un pauc seriós !
Pepper|5|True|Per pietat !
Pepper|6|False|Sonque per uèi !
Cònsol de Komona|7|True|Masca de la magia espirituala e ascetica de « Ah », es un plaser d'aver amb nosautres...
Cònsol de Komona|8|False|SHICHIMI !
Pepper|10|False|Carròt ! NON !!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
Public|7|False|Ha !
Cònsol de Komona|1|False|Ara, es lo torn d'aquela participanta misteriosa, que s'es marcada de faiçon anonima per representar la magia flamejanta de « Magmah »...
Cònsol de Komona|3|True|ÒU ! Quala suspresa ! Òc, es plan ela ! S'agís de...
Cònsol de Komona|4|False|SAFRAN !
Son|2|False|Flap !
Cònsol de Komona|6|False|Tè ! Semblariá qu'una participanta aja ja un problèma !
Son|5|False|Flòc !
Cònsol de Komona|8|False|Madomaisèla !! Vos pregam de tornar a vòstra plaça !!
Pepper|9|False|Caaarròtt ! NON !
Son|10|False|Dzin !
Pepper|11|False|?!
Son|12|False|Plau !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ha-ha ! Mercé, Espirulina ! E desolada...
Pepper|2|True|Ont vas, tu ?!
Pepper|3|False|TORNA !
Cònsol de Komona|4|True|Em ! Vaquí doncas nòstra darrièra participanta, de la magia de... bè... l'inqualificable « Caosah »...
Cònsol de Komona|5|False|PEPPER !
Cònsol de Komona|6|False|Coma granda ganhanta del concors de pocion, avèm decidit de li balhar una plaça de tria !
Pepper|7|False|?!...
Cònsol de Komona|8|False|D'efièit, farà...
Cònsol de Komona|9|False|... partida de nòstra jurada !
Son|10|False|Pom
Public|11|True|Clap
Public|12|True|Clap
Public|13|True|Clap
Public|14|True|Clap
Public|15|False|Clap
Escritura|16|False|Concors de Magia Jurada Officiala
Pepper|17|False|Genial.
Escritura|18|False|Reina Aiel
Escritura|19|False|Pepper
Escritura|20|False|Lord Azeirf
Narrator|21|False|DE SEGUIR...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|02/2017 - www.peppercarrot.com - Dessenh & Scenari : David Revoy
Crèdits|2|False|Brainstorming : Craig Maloney, Quiralta, Nicolas Artance, Talime e Valvin.
Crèdits|3|False|Melhorament dels dialògues : Craig Maloney, Jookia, Nicolas Artance e Valvin.
Crèdits|4|False|Mercejaments : la còla d'Inkscape, mai que mai Mc.
Crèdits|5|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Crèdits|6|False|Logicials : Krita 3.2.1, Inkscape 0.91 sus Linux Mint 18.1 XFCE
Crèdits|7|False|Licéncia : Creative Commons Attribution 4.0
Crèdits|9|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 816 mecènas :
Crèdits|8|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
