# Transcript of Pepper&Carrot Episode 06 [cn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
标题|1|False|第6集：魔药大赛

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|True|糟糕，我又忘记睡觉前 把窗户关上了…
小辣椒|2|True|…好大的风！
小辣椒|3|False|…可我为什么能从窗口看到科魔那城市呢？
小辣椒|4|False|科魔那！
小辣椒|5|True|魔药大赛！
小辣椒|6|True|我……我一定是 不小心睡着了！
小辣椒|9|True|等一下…
小辣椒|10|False|我这是 在哪儿?!?
小鸟|12|False|嘎？|nowhitespace
小辣椒|7|False|*|nowhitespace
注释|8|False|* 参见第4集：灵光一闪

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|False|!!!
小辣椒|2|False|萝卜头！ 你能想到带我去大赛现场，你真是太可爱了！
小辣椒|3|False|让-人-佩-服！
小辣椒|4|True|你甚至还想到了带上 一瓶魔药，我的衣服 和帽子……
小辣椒|5|False|让我看看你带的 是哪瓶魔药……

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|False|什么？！！
科魔那市长|3|False|身为科魔那市长，我宣布魔药大赛拉开序幕！
科魔那市长|4|False|即使是第一届魔药大赛， 本市依然迎来了 至少四位魔女参赛
科魔那市长|5|True|请大家鼓掌
科魔那市长|6|False|热烈欢迎：
书写|2|False|科魔那市魔药大赛

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
观众|30|False|啪
科魔那市长|1|True|来自科技联盟大国， 我们有幸迎来乐天 且技艺精湛的
科魔那市长|3|True|…也不要忘记我们本国的选手，来自科魔那本土的魔女
科魔那市长|5|True|…第三位向大家介绍的参赛者是来自 于月落之国的
科魔那市长|7|True|…最后，我们的最后一位参赛者是来自于 松鼠尾森林的
科魔那市长|2|False|香菜叶！
科魔那市长|4|False|藏红花！
科魔那市长|6|False|七味粉！
科魔那市长|8|False|小辣椒！
科魔那市长|9|True|比赛开始！
科魔那市长|10|False|比分由观众掌声 热烈程度而定！
科魔那市长|11|False|首先，请香菜叶做 她的魔药展示
香菜叶|13|False|女士们， 先生们…
香菜叶|14|True|…无需再惧怕死亡， 只要有了…
香菜叶|15|True|…我的魔药
香菜叶|16|False|行尸走肉！
观众|17|True|啪
观众|18|True|啪
观众|19|True|啪
观众|20|True|啪
观众|21|True|啪
观众|22|True|啪
观众|23|True|啪
观众|24|True|啪
观众|25|True|啪
观众|26|True|啪
观众|27|True|啪
观众|28|True|啪
观众|29|True|啪

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
科魔那市长|1|False|精彩绝伦！
科魔那市长|2|False|香菜叶用她神奇的魔药 挑战了死亡本身！
观众|3|True|啪
藏红花|17|True|请看
藏红花|16|False|…等一下，科魔那的 市民们，请保留 你们的掌声
藏红花|20|True|一瓶你们所有人都在 等待的真正的魔药： 它能让你的邻居们 对你刮目相看…
藏红花|21|False|…甚至 心怀嫉妒！
藏红花|18|True|我的
藏红花|24|False|大富大贵！
藏红花|23|True|…魔药
藏红花|22|True|…现在将这一切成为可能， 非常简单，只要一滴我的…
观众|4|True|啪
观众|5|True|啪
观众|6|True|啪
观众|7|True|啪
观众|8|True|啪
观众|9|True|啪
观众|10|True|啪
观众|11|True|啪
观众|12|True|啪
观众|13|True|啪
观众|15|False|啪
观众|26|True|啪
观众|27|True|啪
观众|28|True|啪
观众|29|True|啪
科魔那市长|44|False|这瓶魔药可以使科魔那 全城富起来！
科魔那市长|43|True|精彩绝伦！不可思议！
观众|32|True|啪
观众|33|True|啪
观众|34|True|啪
观众|35|True|啪
观众|36|True|啪
观众|37|True|啪
观众|38|True|啪
观众|39|True|啪
观众|40|True|啪
观众|41|True|啪
观众|42|False|啪
观众|50|True|啪
观众|51|True|啪
观众|52|True|啪
观众|53|True|啪
观众|54|True|啪
观众|55|True|啪
观众|56|True|啪
观众|57|True|啪
观众|58|True|啪
观众|59|True|啪
观众|60|True|啪
观众|61|True|啪
观众|62|True|啪
观众|63|True|啪
观众|64|True|啪
观众|65|True|啪
观众|66|True|啪
观众|67|True|啪
观众|68|False|啪
科魔那市长|45|False|你们的掌声不会错， 香菜叶已经被比下去啦
藏红花|19|False|魔药

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
科魔那市长|1|False|想要扭转现场局势对于七味粉来说会有些困难！
七味粉|4|True|不可以！
七味粉|5|True|我不能这样做， 这样会太危险了
七味粉|6|False|实在抱歉！
科魔那市长|3|False|…来吧，七味粉， 大家都等着呢
科魔那市长|7|False|女士们，先生们，七味粉看来是要放弃比赛了……
藏红花|8|False|拿来吧！
藏红花|9|False|…不要这么胆小， 浪费了表演机会
藏红花|10|False|再者说，大家都知道 无论你的魔药效果如何，大赛的获奖者都将会是我……
七味粉|11|False|!!!
声音|12|False|噼里啪啦噼里啪啦|nowhitespace
七味粉|15|False|大型怪兽！
七味粉|2|False|我……我不知道 还要进行 现场展示
七味粉|13|True|小心！！！
七味粉|14|True|这瓶魔药是

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小鸟|1|False|咯咯咯………………|nowhitespace
声音|2|False|呯！
小辣椒|3|True|…呃，那好！
小辣椒|5|False|我的魔药应该至少值得你们笑一笑，因为……
小辣椒|4|False|那现在就轮到我了吗？
科魔那市长|6|True|傻瓜，快逃吧
科魔那市长|7|False|魔药大赛结束了！ …保命要紧！

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|2|False|…每次都是这样， 一轮到咱们 大家就都散了
小辣椒|1|True|我就知道：
小辣椒|4|True|不过，我倒是 有个主意， 可以试试你的“魔药” 萝卜头
小辣椒|5|False|…把这里处理好 我们就回家！
小辣椒|7|True|你这个
小辣椒|8|False|大富大贵的行尸走肉 大型怪兽咕咕鸟！
小辣椒|10|False|你有兴趣试试 这最后一瓶魔药吗？
小辣椒|11|False|不太感兴趣吧， 嗯？
小辣椒|6|False|嘿！
声音|9|False|咔嚓！|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|True|没错，看清楚 这个标签…
小辣椒|2|False|你要是不立刻离开科魔那，我就会毫不犹豫地把这瓶魔药倒在你身上！
科魔那市长|3|True|鉴于她把我们的城市 从灾难中解救出来，
科魔那市长|4|False|因此我们将魔药大赛 的冠军授予小辣椒和 她的魔药…？！！
小辣椒|7|False|…呃…其实， 这并不是一瓶真正的魔药， 而是我的猫尿检用的样品！
小辣椒|6|True|…哈哈哈！ 是的…
小辣椒|8|False|…不用展示了吧 …嗯？
旁白|9|False|第6集：魔药大赛
旁白|10|False|完
书写|5|False|50 000 Ko
参与者名单|11|False|2015年3月 - 绘图及脚本：David Revoy 校对：Aurélien Gâteau - 翻译：庄冉

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
参与者名单|1|False|小辣椒与萝卜头是完全免费且开源的，项目资金由读者捐助。 本集有245位捐助者：
参与者名单|4|False|https://www.patreon.com/davidrevoy
参与者名单|3|False|您也可以成为下一集小辣椒与萝卜头的捐助者：
参与者名单|7|False|工具：本集100%由开源软件绘制 Linux Mint环境下Krita
参与者名单|6|False|开源：所有资源，使用字体，图层文件均可在官方网站上免费下载
参与者名单|5|False|授权许可：Creative Commons Attribution 您有权修改，分享，发售，等等……
参与者名单|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
