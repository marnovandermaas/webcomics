# Transcript of Pepper&Carrot Episode 06 [go]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Eqerouey 6 : La pocioneriy

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Bricht, je sei core restae san cllore la coêzey…
Pepper|2|True|...maez qheu vent !
Pepper|3|False|...e pourqhi qe je vais Komona par la coêzey ?
Pepper|4|False|KOMONA !
Pepper|5|False|La pocioneriy !
Pepper|6|False|J'ae deù m'endormir san sonjer !*
Pepper|9|True|…maez ?
Pepper|10|False|Eyou qe je sei rendu, la ?!?
Bird|12|False|N?|nowhitespace
Bird|11|True|coi|nowhitespace
Note|7|False|* vair éqerouey 4 : une belle hate

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carote ! T'és trop boudet d'avair sonjae de m'amener ao concours !
Pepper|3|False|Ma-gni-fi-cent !
Pepper|4|True|T'as memme sonjae de prendr une pocion, mez hardes e mon chapèo...
Pepper|5|False|... vayom vair qheu pocion qe t'as prinz...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|QHI ?!!
Mayor of Komona|3|False|Je décllere, come maere de Komona, la pocioneriy entamey !
Mayor of Komona|4|False|Notr vile ét vrae oure d'aeze de recevair pas meins de catr encraodoeres pour la permiere-ici
Mayor of Komona|5|False|Tapez ben fort dez maens pour yeùz :
Writing|2|False|Pocioneriy de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|30|False|Clap
Mayor of Komona|1|True|Ol ét venue du grand payz dez Tecnolojists, je som vrae benaeze de recevair
Mayor of Komona|4|True|… san oublir notr filhe du payz q'ét de Komona
Mayor of Komona|6|True|... la treizieme a prendr part vient dez payz dez lunes a se coùcher
Mayor of Komona|8|True|... pour finir, la darraene vient dreit bout de la foreyt de Fin-fond-du-Fond
Mayor of Komona|5|False|Safran !
Mayor of Komona|7|False|Shichimi !
Mayor of Komona|9|False|Pepper !
Mayor of Komona|10|True|Le concours ét a comencer !
Mayor of Komona|11|False|Le vote sera a la cllapey de maens !
Mayor of Komona|12|False|Pour comencer, Coriandr a montrer son tourn de fiziqe
Coriander|13|False|Mezdames e Messieus...
Coriander|14|False|… y a pus afaere de s'éfoùcher de la charrte de David grâce a…
Coriander|15|True|... ma pocion de
Coriander|16|False|REVILACION !
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Mayor of Komona|2|True|Coriandr
Mayor of Komona|3|False|q'ét si moêze od tant d'aobiche !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|DE PER-MI-E-RE !
Mayor of Komona|2|False|Coriandr encrouye la charrte de David od la pocion mi-ra-qhu-leuze-ilae !
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|True|Clap
Audience|17|False|Clap
Saffron|19|True|qer vaici
Saffron|18|False|... maez, tout come, decessez de taper dez maens, monde de Komona !
Saffron|22|True|La vrae pocion qe v'étes tertous a esperer : yelle qi va émayer tous voz vaizins ...
Saffron|23|False|... pousser une jalouziy cez yeùz !
Saffron|20|True|MA
Saffron|26|False|BOURJOÊZERIY !
Saffron|25|True|... pocion de
Saffron|24|True|... y ara moyen maezei ren q'od une falhiy goute de ma…
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|True|Clap
Audience|42|False|Clap
Audience|41|True|Clap
Mayor of Komona|44|False|Od la pocion-la, je serom riche tertous a Komona !
Mayor of Komona|43|True|Mirobolant ! Pas creyabl !
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Audience|3|True|Clap
Audience|27|True|Clap
Mayor of Komona|45|False|Y a pas a termayer a vouz ouair taper dez maens : Coriandr ét deja hors de boce.
Saffron|21|False|pocion a mai

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Astoure-ci, ça qi va étr malaezae a Shichimi pour étr de talhe !
Shichimi|4|True|NOUNA !
Shichimi|5|True|Je peuz pas, trop riscae
Shichimi|6|False|PARDON !
Mayor of Komona|3|False|... Shichimi, alom don, tertous sont a esperer
Mayor of Komona|7|False|A pareytr, mezdames e messieus, Shichimi seraet a se dédire…
Saffron|8|False|Doune-mai ella !
Saffron|9|False|... e peis decesse de faere ta nozouze e gâche pas le pestacl
Saffron|10|False|Tout le monde sont deja a savair qe j'ae gaegnae le concours aloure n'importe qhi qe ta pocion fera …
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|monstr maçacr !
Shichimi|2|False|Je... Je savaes pas mai qe falhaet faere une amontrey ...
Shichimi|13|True|GARE !!!
Shichimi|14|True|'ét une pocion de

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CRi CRi CRiiii ii ii ii ii ii ii iii i ii|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|... heu tranqhile !
Pepper|5|False|... ma pocion devraet meriter ao meins de vouz mettr a rire
Pepper|4|False|ét mon tourn a mai don astoure ?
Mayor of Komona|6|True|Saove-tai bobiane !
Mayor of Komona|7|False|le concours ét finit ! ...saove ta piao !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...come lez aotrs fais, tout le monde se saovent cant c'ét a nouz
Pepper|1|True|e v'la :
Pepper|4|True|En tous lez cas, j'ae une petite sonjance sus ce qe je vom pouair faere od ta "pocion" Carote
Pepper|5|False|...remettom tout le cai ici en ordr e rentrom-nouz !
Pepper|7|True|Tai le
Pepper|8|False|qhui-qhui-maçacr-mort-vivant-bourjoêz !
Pepper|10|False|Ella te haeteraet d'assaeyer la darraene pocion ?…
Pepper|11|False|... t'és pas jeïney, haou ?
Pepper|6|False|HAO !
Sound|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ver, liz ben sus l'étiqhette…
Pepper|2|False|... je resterae pas entr lez deuz pour en gâter sus tai si tu décanilhes pas dés taloure de Komona !
Mayor of Komona|3|True|Caoze q'ol a saovae notr vile de la peiroulhe,
Mayor of Komona|4|False|je rebalhom la permiere pllace a Pepper pour sa pocion de ...??!!
Pepper|7|False|... heu... en vrae, ét pas tafaet une pocion ; 'ét la yotey de date a mon chat pour sa vizite de medecin !
Pepper|6|True|... Haha! yan ...
Pepper|8|False|... pas bezein d'assaeyer, padit ?
Narrator|9|False|Episode 6 : La pocioneriy
Narrator|10|False|V'la le bout
Writing|5|False|50 000 Ko
Credits|11|False|March 2015 - Dessin et Scénario : David Revoy , corrections : Aurélien Gâteau

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carote ét pour ren de cheu en cheu, open-source, e eïdae od lez donaezons dez leizous. Pour l'éqerouey-ci, je remerciom lez 245 donouz :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Vouz etou, devenez donouz de Pepper&Carote pour la perchaene éqerouey :
Credits|7|False|Tools : Cet épisode a été dessiné a 100% avec des logiciels libres Krita sur Linux Mint
Credits|6|False|Open-source : toutes les sources, polices d'écritures, fichiers avec calques sont disponibles sur le site officiel au téléchargement.
Credits|5|False|License : Creative Commons Attribution vous pouvez modifier, repartager, vendre, etc...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
