#!/bin/bash
#
#  check-rendering-prerequisites.sh
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#
#  Copyright 2020 GunChleoc <fios@foramnagaidhlig.net>
#
#  Script for running checks that need to be green for the rendering to work
#

curdir="$(dirname "$0")"

exit_code=0

# Validate title extraction
0_transcripts/collect_titles.py
if [[ ! $? -eq 0 ]] ; then
    echo "ERROR: Unable to collect all episode titles"
    echo " "
    echo "Make sure your title files have an attribute id=\"episode-title\" set"
    echo "for a main-level flowRoot or text tag"
    echo " "
    echo "Note that this will NOT work for nested elements, e.g. tspan tags"
    echo " "
    echo "Open the affected file(s) in Inkscape, select the text element of the title,"
    echo "click 'Edit -> XML Editor', click 'id' under 'Name', change what's in"
    echo "the field at the bottom to 'episode-title', click 'Set' and save."
    echo " "
    exit_code=1
fi

# Check that all JSON files will parse and comply with the specification templates
"$curdir"/validate_json.py
if [ $? -ne 0 ] ; then
    echo "ERROR: JSON files are not OK"
    echo " "
    exit_code=1
fi

# Check for file:/// links in SVG files
"$curdir"/no_filelinks.sh
if [[ ! $? -eq 0 ]] ; then
    exit_code=1
fi

exit $exit_code
