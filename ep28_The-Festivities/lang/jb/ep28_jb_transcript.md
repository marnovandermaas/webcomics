# Transcript of Pepper&Carrot Episode 28 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 28 mo'o lisri le nu salci
<unknown>|2|False|la .piper. joi la .karot.

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|True|.i ca le cabvanci be le nu le'i ci lunra cu se linji
Narrator|2|False|gi'e te gusni fi le melbi be bu'u le malsi pe la no'e morsi
Narrator|3|False|.i fau la'e di'u bu'u le melbi la .koriander. noi pendo mi fa ke'a
Narrator|4|False|co'a nolraitru la mlotca

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|.i zi co'a salci
Narrator|2|False|fa le ro cecmu .e gei so'i nobli vitke ku ne te ka'a le vrici pagbu be la .erevas.
la .piper.|3|False|mo

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|False|.i ua ti du do
la .piper.|3|False|.i mi pu carmi pensi
la .karot.|2|False|.satr. satr.
la .piper.|4|False|.i .ai mi klama le nenri .i lenki bu'u ti
Journalist|6|False|ju'i do ra ti zvati
Journalist|5|False|.e'ecai .i ra klama
Journalist|7|False|doi la .safran. do skicu pei tu'a su'o cinri se va'u lo'e tcidu be fi la djecitsi mlotca
la .safran.|8|False|.ai .i'a
la .piper.|15|False|.y.
la .piper.|16|False|mi badri tu'a da .i'u bu'o pei doi la .karot.
Sound|9|False|lindi|nowhitespace
Journalist|13|True|doi la .safran. mi'e .erevas. zei taxfu karni
Sound|10|False|lindi|nowhitespace
Sound|11|False|lindi|nowhitespace
Sound|12|False|lindi|nowhitespace
Journalist|14|False|ma finti le ca se dasni be do

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|False|ro pendo be mi tai snada .i .au mi su'o so'u va'e le ka go'i
la .piper.|2|False|.i la koriander. nobli
la .piper.|3|False|.i la .safran. ricfu
la .piper.|4|False|.i ji'a .ue dai la .citcimin. simlu le ka tai prane fau tu'a le se cecmu
la .piper.|5|False|.i mi mo .i mi kansa ma
la .piper.|6|False|ti

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .citcimin.|1|False|.piper.
la .citcimin.|2|False|mi pu sivni lebna so'o cidja .au pei

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .citcimin.|1|True|.i cizra fa le nu mi lebna le cidja ca le nu no drata cu zgana
la .citcimin.|2|False|.i ku'i le ctuca be mi'a cu djica le nu mi'a simsa su'o pruxi lo ka na tai nitcu lo'e cidja
la .safran.|3|False|ua do zvati ti
la .safran.|4|True|.i ro mai mi kakne lo ka na se zgana le jansu be lo karni
la .safran.|5|False|.i gau ra mi tatpi
la .koriander.|6|False|le ti'e jansu be lo karni
la .koriander.|7|False|.i pei troci lo ka rivbi lo nu do tavla lo turni ca lo nu do dasni ti voi taxfu le'e stedu
Sound|8|False|.srak. srak.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .koriander.|1|True|.i ta'o doi .piper. mi penmi ro mai le rirni be do
la .koriander.|2|False|.i ri je'a se za'e steci
la .piper.|3|False|.y.
la .koriander.|4|True|.i va'i do tai se funca lo zabna
la .koriander.|5|False|.i ju'o le rirni cu curmi le nu do gasnu ro da poi do djica
la .koriander.|6|False|.i da'i curmi le nu na smaji zutse fau le nu dasni le mapku be le kargu gi'e xanka tu'a le jecta
la .safran.|7|False|.i da'i curmi le nu juxre dansu ne'a lo spaji prenu
la .citcimin.|8|False|.i da'i curmi le nu citka ro sanmi pe le gusta zi'e poi klesi le vrici

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|4|False|to le lisri nu'o mulno toi
la .citcimin.|1|False|oi se'i doi .piper. gau mi'a xu do co'a klaku
la .piper.|2|True|no da nabmi
la .piper.|3|False|.i mi ckire do doi le pendo

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|5|True|.i do ji'a ka'e sidju .i je ba'e le cmene be do ba se ciska fi le tai papri
la .piper.|3|True|la'e la .piper. jo'u la .karot. cu fingubni gi'e se sarji le tcidu
la .piper.|4|False|.i sarji le nu finti le dei lisri kei fa le 960 da poi liste fa di'e
la .piper.|7|True|.i ko tcidu fi la'o .url. www.peppercarrot.com .url. ja'e le nu do djuno fi le tcila
la .piper.|6|True|.i mi'a pilno la'o gy.Patreon.gy. e la'o gy.Tipeee.gy. e la'o gy.PayPal.gy. e la'o gy.Liberapay.gy. e la'o gy. e lo drata
la .piper.|8|False|ki'e do
la .piper.|2|True|xu do djuno le du'u
Credits|1|False|.i de'i li nanca bu 2019 masti bu 1 co'a gubni .i le pixra .e le lisri cu se finti la'o fy. David Revoy.fy. .i pu cipra tcidu lu'i le lisri poi pu nu'o bredi fa la'o gy.CalimeroTeknik.gy. e la'o gy.Craig Maloney.gy. e la'o gy.Martin Disch.gy. e la'o gy.Midgard.gy. e la'o gy.Nicolas Artance.gy. e la'o gy.Valvin.gy. .i le lojbo xe fanva zo'u fanva fa la gleki .i skicu la .erevas. noi munje zi'e noi finti fa la'o.fy.David Revoy.fy. .i ralju sidju fa la'o gy.Craig Maloney.gy. .i finti lei lisri be la .erevas. fa la'o gy.Craig Maloney.gy. e la'o gy.Nartance.gy. e la'o gy.Scribblemaniac.gy. e la'o gy.Valvin.gy. .i cikre le xe fanva fa la'o gy.Willem Sonke.gy. e la'o gy.Moini.gy. e la'o gy.Hali.gy. e la'o gy.CGand.gy. e la'o gy.Alex Gryson.gy. .i pilno le proga poi du la'o py. Krita 4.1.5~appimage.py. jo'u la'o py. Inkscape 0.92.3.py. jo'u la'o py.Kubuntu 18.04.1.py. i finkrali javni fa la'o jy. Creative Commons Attribution 4.0 .jy .i zoi .urli. www.peppercarrot.com .urli. judri
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
