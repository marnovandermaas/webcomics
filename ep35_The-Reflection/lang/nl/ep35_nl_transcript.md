# Transcript of Pepper&Carrot Episode 35 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Aflevering 35: Reflectie

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ah, je bent weer wakker!
Pepper|2|False|Gaat het? Je hebt het toch niet te koud?
Pepper|3|True|Sorry, mijn Rea raakt bijna helemaal op,
Pepper|4|False|en mijn aura is te zwak om ons te beschermen tegen de kou op deze hoogte.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ik sta op het punt mijn supersnelheid te verliezen,
Pepper|2|True|en die draak en drakenrijder achter-volgen ons al uren.
Pepper|3|False|Ze zullen geen enkel probleem hebben ons te vangen zodra we langzamer gaan.
Pepper|4|True|Hoe kon ik zo onnozel zijn!
Pepper|5|False|Ik was er zeker van dat ze het uren geleden al opgegeven zouden hebben!
Pepper|6|False|Kon ik ze maar van me afschudden.
Pepper|7|True|NEE . Ik moet van ze af komen!
Pepper|8|False|Voordat ze ons vangen!
Pepper|9|True|Maar ik ben zó...
Pepper|10|True|...ontzettend...
Pepper|11|False|...moe...
Pepper|12|True|OEPS!
Pepper|13|False|Wel je hoofd erbij houden, Pepper!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Zag jij dat ook?
Arra|2|True|Jazeker. Deze achtervolging is zo voorbij.
Arra|3|False|Het duurt niet lang meer voordat haar krachten uitgeput zijn.
Torreya|4|False|En dát is het moment dat we haar aanvallen.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|BoOE M !
Pepper|2|False|WAT?!
Pepper|3|False|Ze zijn door de geluidsbarrière heen gebroken?!
Pepper|4|False|! ! !
Pepper|5|False|WOoesj ! !!
Pepper|6|False|Grrr!
Pepper|7|False|Zet je schrap, Carrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|DROMMELS!
Pepper|2|False|WAT NU?!
Pepper|3|False|Ooh!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|De grot van het Hoornmeer!
Pepper|2|False|Carrot, dit is onze kans!
Pepper|3|False|Ik ken deze grot.
Pepper|4|True|Het is een kronkelende gang die uitloopt op een ingestorte zaal.
Pepper|5|True|De stalactieten aan het plafond van die zaal vormen nu een muur van spiezen. Het is een dodelijke val!
Pepper|6|True|Er is een kleine opening die net groot genoeg is om ons er doorheen te wurmen,
Pepper|7|False|maar de draak zal nooit op tijd kunnen stoppen om niet op hoge snelheid op de muur in te vliegen! Mwahahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Perfect! Ze volgen ons!
Pepper|2|False|We zullen ons zo van hen verlost hebben!
Pepper|3|True|Ik kan niet wachten om dat te zien!
Pepper|4|False|Mwahahaha!
Pepper|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Wacht... wat is er van me geworden?
Pepper|2|True|Een moordenaar?
Pepper|3|False|Een gemene heks?
Pepper|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|SWooesj!
Pepper|2|True|NEE!
Pepper|3|False|Dat is niet wie ik ben!
Pepper|4|True|STOOOOP!!!
Pepper|5|False|IK GEEF ME OVER!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Je snapt natuurlijk wel dat jezelf overgeven aan Meester Wasabi niet goed voor je zal eindigen, toch?
Pepper|2|False|Dat weet ik, maar ik voel dat er een manier moet zijn om tot haar door te dringen, om begrip te kweken.
Pepper|3|True|Want niets is immers onherstelbaar.
Pepper|4|False|Dat heeft een goede vriendin mij geleerd.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Bedankt dat Carrot wat schone kleren voor me mocht gaan halen.
Torreya|2|True|Geen probleem.
Torreya|3|False|Je hebt het me een stuk gemakkelijker gemaakt, dus het was het minste dat ik kon doen.
Torreya|4|True|Maar weet je, je hebt Wasabi behoorlijk kwaad gemaakt.
Torreya|5|True|Ze eist perfectie. Je hebt dat vervuild met je viezigheid, je kleding, en je aankomst.
Torreya|6|False|En je hebt haar nog aangevallen ook! Ik zou geen medelijden verwachten.
Pepper|7|False|Dat snap ik.
Torreya|8|True|Maar goed, ik heb een dutje nodig.
Torreya|9|True|We vertrekken wanneer ik wakker ben.
Torreya|10|False|Doe je eigen ding zolang je maar in de buurt blijft...
Geluid|11|False|Plens
Torreya|12|False|Wat een bijzondere heks.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ongeacht het risico en het gevaar zal ik vanaf nu
Pepper|2|True|trouw blijven aan wie ik ben.
Pepper|3|False|Beloofd!
Verteller|4|False|WORDT VERVOLGD...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|18 juni 2021 Tekeningen & verhaal: David Revoy. Bèta-feedback: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Nederlandse versie Vertaling : Julien Bertholon. Proeflezing: Marno van der Maas. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.2, Inkscape 1.1 op Kubuntu Linux 20.04 Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|2|True|Wist je dat?
Pepper|3|True|Pepper&Carrot is helemaal vrij,open-bron en gesponsord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik 1054 patronen!
Pepper|5|True|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|6|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay ... en meer!
Pepper|7|True|Kijk op www.peppercarrot.com voor alle info!
Pepper|8|False|Dank je!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
