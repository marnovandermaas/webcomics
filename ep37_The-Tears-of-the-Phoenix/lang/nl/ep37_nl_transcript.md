# Transcript of Pepper&Carrot Episode 37 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 37: De fenikstranen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Poeh...
Pepper|2|False|Fijn om hier te zijn!
Pepper|3|False|O, het is marktdag.
Pepper|4|False|Laten we eerst iets eten voordat we de vulkaan beklimmen.
Pepper|5|False|Ik wist dat je dat leuk zou vinden!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geschrift|1|False|GEZOCHT
Geschrift|2|False|Torreya
Geschrift|3|False|1 00 000Ko
Geschrift|4|False|Shichimi
Geschrift|5|False|250 000Ko
Geschrift|6|False|Pepper
Geschrift|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ze hebben dit echt overal opgehangen.
Pepper|2|False|Zelfs op de meest afgelegen plaatsen.
Pepper|3|False|Kom, laten we gaan voordat we herkend worden.
Pepper|4|False|We hebben grotere problemen op het moment...
Pepper|5|False|Dat licht...
Pepper|6|False|We zijn vast dichtbij het nest.
Pepper|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Feniks|1|False|Waarom stoor je mij, mens?
Pepper|2|False|Gegroet, O grote Feniks!
Pepper|3|False|Ik heet Pepper, en ik ben een heks van Chaosah.
Pepper|4|False|Ik heb een grote dosis Drakenrea ingenomen en sindsdien kwam dit op en blijft elke dag groeien...
Pepper|5|False|Het neutraliseert al mijn krachten en is mogelijk dodelijk.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Feniks|1|True|Hmm...
Feniks|2|False|Oké...
Pepper|3|False|...en nu ben je hier omdat je een Fenikstraan nodig hebt om te genezen, toch?
Pepper|4|False|Ja, het is mijn enige optie.
Feniks|5|False|*zucht*
Feniks|6|False|...nou, waar wacht je op?
Feniks|7|False|Laat me huilen dan.
Feniks|8|False|Ik geef je één minuut!
Pepper|9|False|Wat?!
Pepper|10|False|Laten huilen?!
Pepper|11|False|Jeetje, dat wist ik niet...
Pepper|12|False|Ik bedoel... Maar één minuut?!
Pepper|13|True|Um...
Pepper|14|True|Oké!
Pepper|15|False|Even kijken.
Pepper|16|True|Umm... Denk aan wereldhonger.
Pepper|17|True|Uh, wacht wacht!
Pepper|18|True|Ik heb nog iets beters:
Pepper|19|False|Dode dierbaren.
Pepper|20|True|Nog niks?
Pepper|21|True|Verlaten huisdieren?!
Pepper|22|False|Verlaten huisdieren zijn zo triest...
Feniks|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Hoe was dat?
Pepper|2|False|Moet je daar niet om huilen?
Feniks|3|False|DIT MEEN JE NIET?!
Feniks|4|False|IS DAT ALLES?!
Feniks|5|True|Zij kwamen met gedichten!
Feniks|6|True|Schreven tragedies!
Feniks|7|True|KUNST!
Feniks|8|False|TONEEL!
Feniks|9|True|MAAR JIJ?!
Feniks|10|False|KOMT ZONDER VOORBEREIDING!
Feniks|11|True|MAAK DAT JE WEG KOMT!
Feniks|12|False|KOM TERUG WANNEER JE JE HEBT VOORBEREID!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grr! Kom op, Pepper! Bedenk een triest verhaal.
Pepper|2|True|Je kunt het.
Pepper|3|False|Je kunt het.
Pepper|4|True|Nee...
Pepper|5|False|Je faalt.
Verkoper|6|False|Hee! Oi! Ga weg!
Pepper|7|False|!!
Verkoper|8|False|Blijf met je poten van mijn spullen af als je niet betaalt!
Pepper|9|True|Oh nee, waag het niet...
Pepper|10|True|CARROT!
Pepper|11|False|Je kunt me helpen in plaats van alleen maar aan eten te denken!
Pepper|12|False|O?!
Pepper|13|True|O ja...
Pepper|14|False|Dat zou kunnen.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Feniks|1|True|Aa! Jij weer.
Feniks|2|False|Zo snel...
Feniks|3|False|Een metalen wapen?!
Feniks|4|False|Echt?!
Feniks|5|False|Weet je niet dat ik metaal kan smelten...
Geluid|6|False|Plop
Geluid|7|False|Plop
Geluid|8|False|Plop
Feniks|9|True|O NEE!
Feniks|10|False|NIET DAT!
Feniks|11|False|DAT IS NIET EERLIJK!
Pepper|12|True|Snel Carrot!
Pepper|13|False|Vang zo veel tranen als je kunt!
Geluid|14|False|Hak!
Geluid|15|False|Hak!
Geluid|16|False|Hak!
Titel|17|False|- EINDE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|3 augustus, 2022 Tekeningen & verhaal: David Revoy. Beta lezers: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Nederlandse versie Vertaling: Marno van der Maas. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|2|False|Wist je dat?
Pepper|3|False|Pepper&Carrot is helemaal vrij, open-bron en gesponsoord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik 1058 patronen!
Pepper|5|False|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|6|False|Je kan doneren met Patreon, Tipeee, Paypal, Liberapay ... en meer!
Pepper|7|False|Kijk op www.peppercarrot.com voor alle info!
Pepper|8|False|Dank je!
Notes|9|False|You can also translate this page if you want.
Notes|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Notes|11|False|Beta readers help with the story, proofreaders give feedback about the text.
