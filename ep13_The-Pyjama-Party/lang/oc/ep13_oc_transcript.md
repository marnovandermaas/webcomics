# Transcript of Pepper&Carrot Episode 13 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 13 : La Serada pijama

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Es tras que plan !
Pepper|5|False|Mercé de nos aculhir en cò tieu Coriandre !
Pepper|3|False|...del monde !
Pepper|1|True|Son las melhoras...
Pepper|2|True|...vacanças...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|1|True|Mercé !
Coriandre|2|False|Espèri que sètz pas tròp impressionadas per mon personal.
Pepper|11|False|Non, te'n fagues pas ; son fòrça aculhents, nos sentissèm a nòstre aise sul pic.
Shichimi|13|False|...tan rafinada e tan calorosa a l'encòp !
Pepper|14|False|Aquò's plan verai !
Shichimi|12|True|Ieu soi impressionada per la decoracion...
Montre|9|False|OoooOoo ! ! !|nowhitespace
Montre|10|False|OoOoo ! ! !|nowhitespace
Montre|8|False|OoooOoo ! ! !|nowhitespace
Son|7|False|chooooooiiing !|nowhitespace
Son|6|False|Claf !|nowhitespace
Son|5|False|Clucf !|nowhitespace
Son|3|False|Dzzziii ! ! !|nowhitespace
Son|4|False|Chcac ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|2|True|De còps tròbi tot aquò tan...
Shichimi|5|True|Ieu m'agradariá de viure dins aqueste confòrt,
Son|7|False|Chiingz ! ! !|nowhitespace
Son|4|False|bim !|nowhitespace
Shichimi|8|False|...« Una-vertadièra-masca-de-Ah-deu-pas-viure-atal » .
Pepper|9|True|Ha ha !...
Pepper|10|True|Lo pes de las tradicions...
Pepper|11|False|Sembla una frasa de mas mairinas !
Shichimi|12|False|Vertat ?
Coriandre|1|True|Es plan aimable de m'o rapelar.
Coriandre|3|False|...« banal » ?...
Shichimi|6|False|mas ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|2|False|Anem dròllas, nos tornam concentrar, qu'arribam al cap
Pepper et Shichimi|1|False|Hi hi hi !
Pepper|3|True|Ò,va plan... !
Pepper|5|False|...e avèm quitament pas començat a...
Son|6|False|FRòòòUuuòuu ! ! !|nowhitespace
Son|9|False|Chhchh|nowhitespace
Shichimi|8|False|?!!
Coriandre|7|False|PEPPER ! !!|nowhitespace
Pepper|4|True|Aquesta quista demòra subreaisida...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|6|False|C R A C ! !|nowhitespace
Shichimi|8|False|Ò NON !
Coriandre|9|False|Pas encara !
Pepper|11|False|Daissa aquò ! Sul pic !
Coriandre|1|False|...Ò non ! Es tròp tard ! Es... es...
Shichimi|2|False|N oo oo on !!!|nowhitespace
Mostre|3|False|MOAH HAHA HAHA !!!|nowhitespace
Shichimi|4|False|G RR ! ! !|nowhitespace
Coriandre|5|False|VAS PAGAR !...
Son|7|False|CLING ! !|nowhitespace
Son|13|False|P LO NC ! !|nowhitespace
Son|12|False|P AF ! !|nowhitespace
Pepper|10|True|CARRÒT !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|8|False|- FIN -
Crèdits|9|False|11/2015 - Dessenh & Scenari : David Revoy, correccions: Jo, Frémo
Son|1|False|Pam
Pepper|4|True|Sabi que fas aquò per me defendre...
Pepper|3|True|Anem, romegues pas !
Pepper|5|False|...mas quand jogam servís pas a res !
Carròt|7|False|Grrrr
Escritura|6|False|Citadelles & Phoénix
Escritura|2|False|Princessa Coriandre

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot est completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 602 mecènas :
Crèdits|2|True|Vos tanben, venètz mecèna de Pepper&Carrot per l'episòdi venent sus
Crèdits|4|False|Licéncia : Creative Commons Attribution 4.0 Sorsas : disponiblas sus www.peppercarrot.com Logicials : aqueste episòdi foguèt dessenhat a 100% amb de logicials liures Krita 2.9.9, Inkscape 0.91 sus Linux Mint 17
Crèdits|3|False|https://www.patreon.com/davidrevoy
