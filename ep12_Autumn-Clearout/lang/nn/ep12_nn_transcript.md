# Transcript of Pepper&Carrot Episode 12 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 12: Haustreingjering

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|9|True|Kling
Lyd|10|True|Klang
Lyd|11|False|Klong
Kajenne|14|False|Rydd vekk alt dette så lenge. Det kan gå riktig gale dersom nokre små rakkarar finn flaskene og set seg til å leika med dei.
Kajenne|1|False|Skrattebrygg
Kajenne|2|False|Mega-hårvekst-eliksir
Kajenne|3|False|Stinkbombe-trylledrikk
Kajenne|4|False|Livet-gjennom-rosa-briller-eliksir
Kajenne|5|False|Farga røyk på flaske ...
Kajenne|6|False|... berre for å nemna nokre få!
Kajenne|13|False|Eg må ein liten tur innom marknaden i Komona.
Kajenne|12|False|Nettopp! Det er best slik.
Pepar|7|False|Ja då – eg veit det:
Pepar|8|False|«Ei-ekte-kaosah-heks-ville-aldri-funne-på-å-laga-slike-trylledrikkar!»

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|2|False|Svisj!|nowhitespace
Pepar|3|False|Søren!
Pepar|4|True|Grava det ned?!
Pepar|5|False|Det kjem til å ta fleire timar, og me får sikkert blemmer på nevane av alt arbeidet!
Lyd|6|False|Skubb
Pepar|1|False|Rydda alt vekk? Superenkelt! Jippi!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|«Og ikkje finn på å bruka magi! Ei ekte kaosah-heks brukar aldri magi til husarbeidet.»
Pepar|2|False|Grrr!
Pepar|3|True|GULROT!
Pepar|4|False|Finn bind 7 av Kaosah-bøkene – «Tyngdefelt» – og skund deg!
Lyd|6|True|Tapp
Lyd|7|False|Tapp
Pepar|5|False|Eg skal nok visa den innpåslitne gamle kråka korleis ei ekte kaosah-heks ordnar ting!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|2|False|VRO O oo oo oo oo oo|nowhitespace
Kajenne|3|False|Eit svart hòl av kaosah-magi ...?!
Pepar|4|False|?!
Kajenne|5|False|Er det verkeleg slik du forstår «ikkje bruk magi»?
Kajenne|7|True|Sjølvsagt ikkje!
Kajenne|8|False|Og bra var det! Det går rett og slett ikkje an å la deg vera åleine eit sekund!
Pepar|1|False|GURGES ATER!
Pepar|6|False|Høyr her! Skulle ikkje du til marknaden?!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|wo oo wo owo owoo|nowhitespace
Lyd|7|False|wo oo wo owo owoo|nowhitespace
Lyd|6|False|Slepp
Kajenne|8|False|Og då går det nettopp slik: Nesten alt vil trekkjast mot stabile omløpsbaner eller Lagrange-punkt – og flyta rundt for alltid.
Gulrot|10|False|?!
Kajenne|2|False|Berre sjå på dette ...
Kajenne|3|False|For lite masse!
Kajenne|4|False|For svak tyngdekrafts-differensial!
Kajenne|5|False|Og sjølv den næraste stabile omløpsbana er for lita!
Kajenne|9|False|Sjølv ein litt større hendingshorisont ville vore nok!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|! ! !|nowhitespace
Kajenne|2|False|! ! !|nowhitespace
Kajenne|4|False|FORBANNUS KANSELLARE MAXIMUS!
Lyd|3|False|Sj-Klakk!|nowhitespace
Forteljar|5|False|– SLUTT –
Bidragsytarar|6|False|Oktober 2015 – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer og Arild Torvund Olsen

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 575 som støtta denne episoden:
Bidragsytarar|3|False|https://www.patreon.com/davidrevoy
Bidragsytarar|2|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot:
Bidragsytarar|4|False|Lisens: Creative Commons Attribution 4.0 Kjeldefiler: Tilgjengelege på www.peppercarrot.com Verktøy: Denne episoden er 100 % teikna med fri programvare Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G’MIC 1.6.7 på Linux Mint 17.2
