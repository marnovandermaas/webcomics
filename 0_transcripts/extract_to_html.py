#!/usr/bin/env python3
# encoding: utf-8
#
#  extract_to_html.py
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#
#  Copyright 2019 GunChleoc <fios@foramnagaidhlig.net>
#

import html
import os.path
from pathlib import Path
import sys
from markdown import extract_episode_number, make_transcript_filename, read_transcript


print('######################################################################')
print('#        Tool for converting extracted texts from CSV to HTML        #')
print('######################################################################')

# The first argument is the script name itself, so we expect 3 of them
if len(sys.argv) != 3:
    print('Wrong number of arguments! Usage:')
    print('    0_transcripts/extract_to_html.py <episode> <locale>')
    print('For example:')
    print('    0_transcripts/extract_to_html.py ep01_Potion-of-Flight fr')
    sys.exit(1)

EPISODE = sys.argv[1]
LOCALE = sys.argv[2]


print("Episode '" + EPISODE + "', locale '" + LOCALE + "'")

EPISODE_NUMBER = extract_episode_number(EPISODE)
if EPISODE_NUMBER == '':
    sys.exit(1)

# Get the repository's base directory and navigate to the episode's locale's folder
DIRECTORY = Path(
    os.path.dirname(os.path.abspath(__file__))).parent / EPISODE / 'lang' / LOCALE

# Path for the rendered html files
RENDERINGDIR = Path(
    os.path.dirname(os.path.abspath(__file__))).parent / EPISODE / 'hi-res' / 'html'
if not os.path.exists(RENDERINGDIR):
    os.makedirs(RENDERINGDIR)

# Get markdown transcript filename
TRANSCRIPT_PATH = DIRECTORY / make_transcript_filename(EPISODE_NUMBER, LOCALE)

if not (TRANSCRIPT_PATH.exists() and TRANSCRIPT_PATH.is_file()):
    print('No transcript found for', TRANSCRIPT_PATH.as_posix())
    sys.exit(0)

TRANSCRIPT = read_transcript(TRANSCRIPT_PATH)
if TRANSCRIPT['errors'] > 0:
    print('======================================================================', file=sys.stderr)
    print('Encountered', TRANSCRIPT['errors'], 'error(s)!', file=sys.stderr)
    print('######################################################################', file=sys.stderr)
    sys.exit(1)

print('Directory:', TRANSCRIPT_PATH.parent)

ERRORS = 0
for page in TRANSCRIPT['transcript']:
    print('======================================================================')
    print('Creating HTML for P' + page)

    # Read page contents and assign sort order
    raw_rows = {}
    for row in TRANSCRIPT['transcript'][page]:
        if row[0] == '<hidden>':
            # Translators' instructions etc., we don't want them in the transcript
            continue
        key = int(row[1])
        if key in raw_rows.keys():
            print('**** ERROR: On page ' + page +
                  ', DUPLICATE POSITION ' + str(key) + '! Rows:\n**** \t' +
                  '|'.join(row) + '\n**** \t' + '|'.join(raw_rows[key]) + ' ****',
                  file=sys.stderr)
            ERRORS = ERRORS + 1
        raw_rows[key] = row

    # Combine data into nodes - for rows that contain "True",
    # the following row ends up in the same entry
    entries = []
    current_rows = []
    row = []
    for key in sorted(raw_rows.keys()):
        row = raw_rows[key]

        current_rows.append(row)

        if row[2] == 'False':
            entries.append(current_rows)
            current_rows = []

    # Ensure we don't accidentally skip the final row
    if current_rows:
        print('**** ERROR: For last row on page ' + page + ', |' +
              row[2] + '| must be |False| for ' + '|'.join(row) + ' ****', file=sys.stderr)
        ERRORS = ERRORS + 1

    # Now generate the HTML code
    contents = '<dl>'
    title = ''
    for entry in entries:
        text = ''
        if entry:
            # Add title whenever somebody else starts speaking/making a noise etc.
            if entry[0][0] != title:
                title = entry[0][0]
                contents = contents + '<dt><strong>' + \
                    html.escape(title) + '</strong></dt>\n'

            # Assemble rows for this entry
            for row in entry:
                # Catch obvious bugs with True/False assignment
                if title != row[0]:
                    print("**** ERROR: Title '" +
                          row[0] + "' On page " + page +
                          " does not match previous title '" + title + "' for " +
                          '|'.join(row) + ' ****', file=sys.stderr)
                    ERRORS = ERRORS + 1
                # Catch rows that lack a speaker
                if row[0] == '<unknown>':
                    print('**** ERROR: On page ' + page +
                          ', please assign a speaker for ' +
                          '|'.join(row) + ' ****', file=sys.stderr)
                    ERRORS = ERRORS + 1

                # Add current row to output text
                if len(row) > 4 and row[4] == 'nowhitespace':
                    # Assembled sounds, bird tweetings etc. don't get blank spaces
                    text = text + row[3].replace(' ', '')
                else:
                    text = text + ' ' + row[3]

                # No more rows will be combined into this output text line,
                # let's add the line and start a fresh one
                if row[2] == 'False':
                    contents = contents + '    <dd>' + \
                        html.escape(text.strip()) + '</dd>\n'
                    text = ''

    contents = contents + '</dl>'

    # Print HTML to file <local>_E<number>P<number>.html
    html_file = RENDERINGDIR.as_posix() + '/' + LOCALE + '_E' + EPISODE_NUMBER + 'P' + page + '.html'
    with open(html_file, 'w', encoding='utf-8', newline='\n') as f:
        f.write(contents)
        print('HTML written to ' + html_file)

print('======================================================================')
if ERRORS > 0:
    print('Encountered', ERRORS, 'error(s)!', file=sys.stderr)
    print('######################################################################', file=sys.stderr)
    sys.exit(1)
else:
    print('Done!')
    print('######################################################################')
