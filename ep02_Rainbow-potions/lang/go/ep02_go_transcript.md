# Transcript of Pepper&Carrot Episode 02 [go]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Eqerouey 2 : Lez pocions arc-en-cieu

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Klok
Sound|6|True|Klok
Sound|7|False|Klok
Writing|1|True|GARE
Writing|3|False|PASSEZ FRANC
Writing|2|True|SORCIÈRE
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|tock|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Klok
Sound|2|True|Klok
Sound|3|False|Klok

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Klok
Sound|2|False|Klok
Sound|3|True|Klok
Sound|4|False|Klok
Sound|5|True|Klok
Sound|6|False|Klok
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M|nowhitespace
Sound|7|True|B|nowhitespace
Sound|8|True|e|nowhitespace
Sound|9|False|urk!|nowhitespace
Sound|10|True|S|nowhitespace
Sound|11|True|S|nowhitespace
Sound|12|True|S|nowhitespace
Sound|13|True|P|nowhitespace
Sound|14|True|l|nowhitespace
Sound|15|True|o|nowhitespace
Sound|16|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lup|nowhitespace
Sound|3|True|B
Sound|4|False|lup|nowhitespace
Sound|7|True|tch|nowhitespace
Sound|6|True|yo|nowhitespace
Sound|5|True|f
Sound|10|True|tch|nowhitespace
Sound|9|True|yo|nowhitespace
Sound|8|True|f
Sound|13|False|tch|nowhitespace
Sound|12|True|yo|nowhitespace
Sound|11|True|f

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|La BD-ci ét open-source e 21 donouz ont dounae lour écot sus
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|En remerciant :
Credits|4|False|fait avec Krita sur GNU/Linux
