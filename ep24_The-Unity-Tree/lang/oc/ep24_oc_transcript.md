# Transcript of Pepper&Carrot Episode 24 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 24 : L'Arbre de l'Unitat

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|5|False|PLAC !|nowhitespace
Cayenne|1|True|Un Arbre de l'Unitat ?
Cayenne|4|False|Sèm de mascas del Caòs ; fasèm pas aquela mena de causas.
Cayenne|6|False|Valriá melhor que trabalhes tos « Sòrtilègis de Destruccion » !
Pepper|7|True|Destruccion ?
Pepper|8|True|En aquela tempora ?
Pepper|9|False|Perqué pas de sortilègis de creacion per cambiar ?
Cayenne|10|False|Lo conselh de las Tres Lunas venent arriba, e tos examèns tanben. Estúdia ! Apren ! Trabalha !! E ARRÈSTA DE TE DISTRAIRE AMB AQUELAS IDÈAS PÈGAS !!!
Cayenne|2|False|Aquí ?!
Cayenne|3|True|NON !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Rau !|nowhitespace
Pepper|5|True|Un ivèrn sens Arbre de l'Unitat ?
Pepper|7|False|E perqué non pas utilizar la magia per lo fabricar d'esperieu ?
Pepper|8|True|Tè ! A mai, poiriái combinar las magias d'Hippiah e de Caosah, seriá inedit !
Pepper|9|True|Un grand arbre verd, claufit d'estelas e de sistèmas solars en miniatura !
Pepper|10|False|Mas òc, seriá perfièit !
Pepper|11|True|E mai que mai
Pepper|12|False|creatiu !
Pepper|13|True|Mas es fòra de question de practicar aquela mena de magia aquí…
Pepper|14|False|…mas mairinas veirián sul còp qu'estúdii pas mos « Sortilègis de Destruccion ».
Carròt|3|False|tap
Son|2|False|Ffff
Pepper|6|False|Jamai !
Pepper|4|False|Grrrrr… !!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|3|False|Frr
Son|17|False|ZiiING
Escritura|22|False|Lo KOMONAN
Pepper|1|True|Levat…
Pepper|2|False|Se me soveni plan…
Pepper|4|False|Òc ! Al capítol suls « Sortilègis de Destruccion », Frigola recomanda de los practicar dins una micrò-dimension. E i a avertiments :
Pepper|16|False|Al trabalh !
Comin|19|False|Aquò seriá pas lo bruit d'una micrò-dimension ?
Son|18|False|Chplomp ! !|nowhitespace
Frigola|20|True|Plan !
Escritura|8|False|« Per de rasons de securitat, tornar PAS los resultats amb vos. »
Escritura|6|False|« Despensar PAS tot son Rea en creant la micrò-dimension. »
Escritura|11|True|« Cal pas esperar CAP d'ajuda exteriora…
Pepper|7|False|Plan solide.
Pepper|10|False|A, vaquí !
Pepper|9|False|Pff… Evident !
Pepper|5|False|Mm…
Escritura|12|False|…DEGUN pòt pas veire çò que fasètz ! »
Frigola|21|False|Espèri que Pepper a plan comprés mos avertiments, dins lo capítol suls « Sortilègis de Destruccion »…
Pepper|15|True|Es perfièit !
Pepper|13|True|Degun ?
Pepper|14|True|De vertat !?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Genial !
Pepper|2|True|Es coma un contenedor isolat del demai del monde, amb sonque la la partida de la bibliotèca que n'ai besonh.
Pepper|3|False|L'endreit perfièit per testar mos sortilègis !
Pepper|4|False|E ara, mon cap-d'òbra !
Son|5|False|ZiiING
Son|6|False|PLooomm ! !|nowhitespace
Son|8|False|Blorf ! !|nowhitespace
Son|10|False|Tching ! !|nowhitespace
Pepper|7|False|Mm… èèè, non.
Pepper|9|False|Fuè !
Pepper|11|False|Òc ! Es plan, mas un pauc tròp petit.
Pepper|13|False|CARRÒT ! TÒQUES PAS !
Carròt|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|FFlomm ! !|nowhitespace
Pepper|3|True|Vesi pas de problèma a tornar aqueste arbre dins lo monde real…
Pepper|2|True|Aup ! A la bona talha. Ara, es perfièit !
Pepper|5|True|Bon !
Pepper|6|True|Es ora de desplegar aquesta micrò-dimension dins la realitat, que vejan quant lo Caòs pòt èstre creatiu !
Son|8|False|Blom ! !|nowhitespace
Frigola|9|True|Tè, Pepper es tornada.
Frigola|10|False|Òsca, Cayenne. Tos metòdes educatius an fait lor efièit.
Cayenne|11|False|Mercé, Mèstra Frigola.
Pepper|7|False|Me triga de véser lor cara !
Escritura|12|False|Lo KOMONAN
Pepper|4|False|Es pas qu'un Arbre de l'Unitat, es pas coma s'èra un « Sortilègi de Destruccion ».

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|5|True|BRRrrr
Son|6|True|BRRr
Son|7|True|BRRrrr
Son|8|True|BRRrrr
Son|9|True|BRRrrr
Son|10|False|BRrr
Carròt|11|False|?
Carròt|13|False|!!
Son|14|True|CRRr
Son|15|False|FRRrrrr
Carròt|16|False|!!
Pepper|1|True|CARRÒT !
Pepper|2|True|Vau quèrre Frigola, Cayenne e Comin.
Pepper|3|False|Tòques pas a res, m'as compresa ?
Pepper|4|False|Torni sul pic !
Pepper|17|True|Preparatz-vos a èstre espantadas et esbalausidas ! Es quicòm d'inedit.
Pepper|18|False|Seguissètz-me al primièr per admirar…
Son|12|False|Frroff !!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|6|False|- FIN -
Son|1|False|Chroff !!
Son|4|False|CRAC ! !!|nowhitespace
Son|3|False|Piauuu ! !!|nowhitespace
Pepper|2|False|?!!
Pepper|5|False|Sembla qu'ai descobèrt un novèl « Sortilègi de Destruccion » !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|2|False|12/2017 - www.peppercarrot.com - Dessenh & Scenari : David Revoy - traduccion : CalimeroTeknik, Nicolas Artance
Crèdits|3|False|Ajuda als dialògues : Craig Maloney, CalimeroTeknik, Jookia.
Crèdits|5|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Crèdits|6|False|Logicials : Krita 3.2.3, Inkscape 0.92.2 sus Ubuntu 17.10
Crèdits|7|False|Licéncia : Creative Commons Attribution 4.0
Crèdits|9|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
Crèdits|8|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 810 mecènas :
Crèdits|4|False|Relectura pendent la version beta : Alex Gryzon, Nicolas Artance, Ozdamark, Zveryok, Valvin e xHire.
Crèdits|1|False|Polidas fèstas !
